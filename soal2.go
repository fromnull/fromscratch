package main

import ("fmt")

//boolean cek :
//true = odd
//false = even

//method 1 using % (slower)
func cekOddEven01(input int)(result bool){
    if (input%2==0){
        return false
    }
    
    return true
}

//method 2 using bitwise (faster)
func cekOddEven02(input int)(result bool){
    if (input & 1 == 0){
        return false
    }
    
    return true
}

func main() {
    fmt.Println(cekOddEven01(2))
    fmt.Println(cekOddEven01(2))
}