package main

import (
            "fmt"
            "net/http"
            
            "goji.io"
            "goji.io/pat"
            "golang.org/x/net/context"
)

func hello (webcontext context.Context, w http.ResponseWriter, r *http.Request){
    name := pat.Param(webcontext,"name")
    fmt.Fprintf(w,"Hello %s", name)
}

func main(){
    mux := goji.NewMux()
    mux.HandleFuncC(pat.Get("/hello/:name"),hello)
    http.ListenAndServe("localhost:8080",mux)
}