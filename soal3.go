package main

import (
        "fmt"
        "math"
)

func smallLarge(arrayInput []int8)(largest, smalles int8){
    smalles = math.MaxInt8
    largest = math.MinInt8
    
    for i := 0; i < len(arrayInput); i++ {
        if (arrayInput[i]>largest){
            largest = arrayInput[i]
        }
        
        if (arrayInput[i]<smalles){
            smalles = arrayInput[i]
        }
    }
    return
}

func main() {
    testArray := []int8 {5,4,3,2,1,99,100}
    
    besar, kecil := smallLarge(testArray)
    
    fmt.Println("Terbesar : ",besar)
    fmt.Println("Terkecil : ",kecil)
}