package main

import (
        "fmt"
        "math"
)

func createResult(input string)(output int){
    output = 0
    for i := len(input) - 1; i >= 0; i-- {
        output = output + (int(input[i] - 48) * int(math.Pow(10,float64(len(input)-i))))
    }
    return output
}

func main() {
    result := createResult("1234")
    fmt.Print(result)
}